#!/bin/bash
to_png()
{
	INPUT_FILE="$1"
	## dir
	DIR="$(dirname "${INPUT_FILE}")"
	FILE="$(basename "${INPUT_FILE}")"
	FILENAME="${FILE%.*}" 
	mkdir -p "$DIR"/"$FILENAME"
	cd "$DIR"/"$FILENAME"
	echo `pwd`	
	ffmpeg -hide_banner -loglevel error -i ../"$FILE"  ./"$FILENAME"_%6d.png
}
export -f to_png
find "$1" -name '*.mov'  -exec bash -c 'to_png "$@"' bash {} \;

